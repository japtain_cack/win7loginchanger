Usage:
======================
1. Place a jpg, that's under 256kb, in the photo directory. I find that sometimes, it has to be less than 256kb. Resolution doesn't seem to matter.
2. Use the next and previous buttons to navigate through your photos.
3. Click apply to set the image as your login background.
4. Use the install or uninstall buttons to add/remove the scheduled task.

* You can access the various folders by right clicking the photo and using the context menu.
* You can refresh the photo database by right clicking the photo and using the context menu.
* A file system watcher continually scans for photos in the photo directory and will update the database if a change is detected.

Command line usage:
======================
    Win7LoginChanger.exe [-i | --install] [-u | --uninstall] [-r | --random] [-q | --quiet]
    
    Install:    Installs the scheduled task
    Uninstall:  Uninstalls the scheduled task
    Random:     Sets the random flag to randomize photo selection
    Quiet:      Changes the background and does not open the GUI,
                no install/uninstall performed during this process.